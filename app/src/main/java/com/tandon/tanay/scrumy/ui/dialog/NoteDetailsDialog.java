package com.tandon.tanay.scrumy.ui.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.tandon.tanay.scrumy.R;
import com.tandon.tanay.scrumy.config.Constants;
import com.tandon.tanay.scrumy.models.view.Note;

/**
 * Created by tanaytandon on 10/07/17.
 */

public class NoteDetailsDialog extends BaseDialogFragment implements View.OnClickListener {

    private Note note;
    private String listName;

    public static NoteDetailsDialog newInstance(Note note, String listName) {
        NoteDetailsDialog dialog = new NoteDetailsDialog();
        Bundle args = new Bundle();
        args.putParcelable(Constants.Keys.NOTE, note);
        args.putString(Constants.Keys.LIST_NAME, listName);
        dialog.setArguments(args);
        return dialog;
    }


    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        // request a window without the listName
        if (dialog.getWindow() != null) {
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null && args.containsKey(Constants.Keys.NOTE)) {
            note = args.getParcelable(Constants.Keys.NOTE);
            listName = args.getString(Constants.Keys.LIST_NAME);
        }
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_note_details, container, false);

        TextView tvNoteTitle = (TextView) view.findViewById(R.id.tv_note_title);
        TextView tvListName = (TextView) view.findViewById(R.id.tv_list_name);
        TextView tvNoteDescription = (TextView) view.findViewById(R.id.tv_note_description);

        if (note != null) {
            tvListName.setText(listName);
            tvNoteDescription.setText(note.description);
            tvNoteTitle.setText(note.title);
        }

        return view;
    }

    @Override
    public void onClick(View view) {

    }
}
