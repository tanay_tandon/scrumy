package com.tandon.tanay.scrumy.database;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.util.LongSparseArray;

import com.tandon.tanay.scrumy.config.AppController;
import com.tandon.tanay.scrumy.models.persistent.DaoSession;
import com.tandon.tanay.scrumy.models.persistent.ListEntity;
import com.tandon.tanay.scrumy.models.persistent.ListEntityDao;
import com.tandon.tanay.scrumy.models.persistent.NoteEntity;
import com.tandon.tanay.scrumy.models.persistent.NoteEntityDao;
import com.tandon.tanay.scrumy.models.view.DataItem;
import com.tandon.tanay.scrumy.models.view.Note;
import com.tandon.tanay.scrumy.ui.listeners.DbOperationListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tanaytandon on 08/07/17.
 */

public class FetchDataTask extends AsyncTask<Void, Void, List<DataItem>> {

    private Context context;
    private DbOperationListener<List<DataItem>> dbOperationListener;

    public FetchDataTask(Context context, DbOperationListener<List<DataItem>> dbOperationListener) {
        this.context = context;
        this.dbOperationListener = dbOperationListener;
    }

    @Override
    protected List<DataItem> doInBackground(Void... voids) {
        List<DataItem> dataItems = new ArrayList<>();
        DaoSession daoSession = ((AppController) context.getApplicationContext()).getDaoSession();

        ListEntityDao listEntityDao = daoSession.getListEntityDao();

        List<ListEntity> listEntities = listEntityDao.loadAll();

        LongSparseArray<DataItem> listIdToDataItemMap = new LongSparseArray<>();

        for (ListEntity listEntity : listEntities) {
            DataItem dataItem = new DataItem(listEntity.getId(),
                    listEntity.getName(), new ArrayList<Note>());

            listIdToDataItemMap.put(listEntity.getId(), dataItem);

            dataItems.add(dataItem);
        }


        NoteEntityDao noteEntityDao = daoSession.getNoteEntityDao();
        List<NoteEntity> noteEntities = noteEntityDao.loadAll();
        for (NoteEntity noteEntity : noteEntities) {
            Note note = new Note(noteEntity.getTitle(), noteEntity.getDescription(),
                    noteEntity.getId(), noteEntity.getListId());
            DataItem dataItem = listIdToDataItemMap.get(noteEntity.getListId());
            dataItem.notes.add(note);
        }


        return dataItems;
    }

    @Override
    protected void onPostExecute(List<DataItem> dataItems) {
        dbOperationListener.onSuccess(dataItems);
    }
}
