package com.tandon.tanay.scrumy.ui.fragment;


import android.app.Activity;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tandon.tanay.scrumy.R;
import com.tandon.tanay.scrumy.config.Constants;
import com.tandon.tanay.scrumy.models.view.DataItem;
import com.tandon.tanay.scrumy.models.view.Note;
import com.tandon.tanay.scrumy.models.view.SList;
import com.tandon.tanay.scrumy.ui.adapter.NoteListAdapter;
import com.tandon.tanay.scrumy.ui.dialog.AddNewNoteDialog;
import com.tandon.tanay.scrumy.ui.dialog.NoteDetailsDialog;
import com.tandon.tanay.scrumy.ui.dialog.SendEmailDialog;
import com.tandon.tanay.scrumy.ui.listeners.RemoveNoteListener;
import com.tandon.tanay.scrumy.ui.listeners.DbOperationListener;
import com.tandon.tanay.scrumy.ui.listeners.NoteCreatedListener;

import java.util.ArrayList;
import java.util.List;


public class NotesFragment extends BaseFragment implements View.OnClickListener,
        View.OnLongClickListener {

    private NoteCreatedListener noteCreatedListener;
    private RemoveNoteListener removeNoteListener;
    private DataItem dataItem;
    private int position;
    public static final String TAG = NotesFragment.class.getName();
    private NoteListAdapter noteListAdapter;
    private List<Note> notes;
    private View btnAddNote;

    public static NotesFragment newInstance(DataItem dataItem, int position) {
        NotesFragment fragment = new NotesFragment();
        Bundle args = new Bundle();
        args.putParcelable(Constants.Keys.DATA_ITEM, dataItem);
        args.putInt(Constants.Keys.POSITION, position);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            dataItem = args.getParcelable(Constants.Keys.DATA_ITEM);
            position = args.getInt(Constants.Keys.POSITION);
        }
        notes = new ArrayList<>();
        if (dataItem != null && dataItem.notes != null) {
            notes.addAll(dataItem.notes);
        }
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup parent,
                             Bundle savedInstanceState) {
        View view = layoutInflater.inflate(R.layout.fragment_notes, parent, false);
        btnAddNote = view.findViewById(R.id.btn_add_new_note);
        btnAddNote.setOnClickListener(this);
        TextView tvListName = (TextView) view.findViewById(R.id.tv_list_name);
        if (dataItem != null) {
            tvListName.setText(dataItem.listName);
        }
        noteListAdapter = new NoteListAdapter(getContext(), notes, this, this);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.rv_notes);
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(noteListAdapter);

        return view;
    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.btn_add_new_note: {
                showAddNewNoteDialog();
                break;
            }
            case R.id.root_view: {
                int tag = (int) view.getTag();
                showNoteDetailsDialog(tag);
                break;
            }
            case R.id.btn_send_email: {
                int position = (int) view.getTag();
                showSendEmailDialog(position);
                break;
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            noteCreatedListener = (NoteCreatedListener) context;
            removeNoteListener = (RemoveNoteListener) context;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case Constants.RequestCodes.NEW_NOTE_CREATED: {
                    Note note = data.getParcelableExtra(Constants.Keys.NOTE);
                    if (noteCreatedListener != null && note != null) {
                        notes.add(note);
                        noteListAdapter.notifyDataSetChanged();
                        noteCreatedListener.onNoteCreated(position, note);
                    }
                }
            }
        }
    }


    private void showAddNewNoteDialog() {
        if (dataItem != null) {
            AddNewNoteDialog addNewNoteDialog = AddNewNoteDialog.newInstance
                    (new SList(dataItem.listId, dataItem.listName));
            addNewNoteDialog.setTargetFragment(this, Constants.RequestCodes.NEW_NOTE_CREATED);
            addNewNoteDialog.show(getFragmentManager(), TAG);
        }
    }

    private void showNoteDetailsDialog(int position) {
        Note note = notes.get(position);
        if (dataItem != null && note != null) {
            NoteDetailsDialog dialog = NoteDetailsDialog.newInstance(note, dataItem.listName);
            dialog.show(getFragmentManager(), TAG);
        }
    }

    @Override
    public boolean onLongClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.root_view: {
                dragNote(view);
                break;
            }

        }
        return false;
    }


    private void showSendEmailDialog(int position) {
        if (notes.size() > position) {
            SendEmailDialog dialog = SendEmailDialog.newInstance(notes.get(position));
            dialog.show(getFragmentManager(), TAG);
        }
    }


    private void dragNote(View view) {
        int notePosition = (int) view.getTag();
        Note note = notes.get(notePosition);
        Intent intent = new Intent();
        Bundle args = new Bundle();
        args.putInt(Constants.Keys.POSITION, position);
        args.putParcelable(Constants.Keys.NOTE, note);
        intent.putExtra(Constants.Keys.ARGS, args);
        ClipData clipData = ClipData.newIntent(note.title, intent);
        View.DragShadowBuilder dragShadowBuilder = new View.DragShadowBuilder(view);
        view.startDrag(clipData, dragShadowBuilder, null, 0);
        removeNoteListener.removeNoteFromCurrentList(this.position, notePosition);
        notes.remove(notePosition);
        noteListAdapter.notifyDataSetChanged();
    }

}
