package com.tandon.tanay.scrumy.models.view;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;


public class DataItem implements Parcelable {

    public Long listId;

    public String listName;

    public List<Note> notes;

    public DataItem(Long listId, String listName, List<Note> notes) {
        this.listId = listId;
        this.notes = notes;
        this.listName = listName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.listId);
        dest.writeString(this.listName);
        dest.writeList(this.notes);
    }

    protected DataItem(Parcel in) {
        this.listId = (Long) in.readValue(Long.class.getClassLoader());
        this.listName = in.readString();
        this.notes = new ArrayList<Note>();
        in.readList(this.notes, Note.class.getClassLoader());
    }

    public static final Parcelable.Creator<DataItem> CREATOR = new Parcelable.Creator<DataItem>() {
        @Override
        public DataItem createFromParcel(Parcel source) {
            return new DataItem(source);
        }

        @Override
        public DataItem[] newArray(int size) {
            return new DataItem[size];
        }
    };
}
