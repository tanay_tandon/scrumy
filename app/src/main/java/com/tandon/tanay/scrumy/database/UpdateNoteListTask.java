package com.tandon.tanay.scrumy.database;

import android.content.Context;
import android.os.AsyncTask;

import com.tandon.tanay.scrumy.config.AppController;
import com.tandon.tanay.scrumy.models.persistent.DaoSession;
import com.tandon.tanay.scrumy.models.persistent.NoteEntity;
import com.tandon.tanay.scrumy.models.persistent.NoteEntityDao;
import com.tandon.tanay.scrumy.models.view.Note;
import com.tandon.tanay.scrumy.ui.listeners.DbOperationListener;

import java.util.List;


public class UpdateNoteListTask extends AsyncTask<Note, Void, Boolean> {

    private Context context;
    private DbOperationListener<Boolean> dbOperationListener;

    public UpdateNoteListTask(Context context, DbOperationListener<Boolean> dbOperationListener) {
        this.context = context;
        this.dbOperationListener = dbOperationListener;
    }

    @Override
    protected Boolean doInBackground(Note... notes) {
        Boolean value = false;
        if (notes != null && notes.length > 0) {
            Note note = notes[0];
            DaoSession daoSession = ((AppController) context.getApplicationContext())
                    .getDaoSession();
            NoteEntityDao noteEntityDao = daoSession.getNoteEntityDao();
            List<NoteEntity> noteEntityList = noteEntityDao.queryBuilder()
                    .where(NoteEntityDao.Properties.Id.eq(note.id)).list();
            if (noteEntityList != null && noteEntityList.size() > 0) {
                NoteEntity noteEntity = noteEntityList.get(0);
                noteEntity.setListId(note.listId);
                noteEntityDao.update(noteEntity);
                value = true;
            }
        }
        return value;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        dbOperationListener.onSuccess(result);
    }
}
