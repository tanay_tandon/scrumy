package com.tandon.tanay.scrumy.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tandon.tanay.scrumy.R;
import com.tandon.tanay.scrumy.models.view.Note;
import com.tandon.tanay.scrumy.ui.viewholder.NoteViewHolder;

import java.util.List;


public class NoteListAdapter extends RecyclerView.Adapter<NoteViewHolder> {

    private Context context;
    private View.OnClickListener clickListener;
    private List<Note> notes;
    private View.OnLongClickListener longClickListener;
    private View.OnDragListener dragListener;

    public NoteListAdapter(Context context, List<Note> notes, View.OnClickListener clickListener,
                           View.OnLongClickListener longClickListener) {
        this.context = context;
        this.notes = notes;
        this.clickListener = clickListener;
        this.longClickListener = longClickListener;
    }

    @Override
    public NoteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                .inflate(R.layout.item_note, parent, false);
        return new NoteViewHolder(view, clickListener, longClickListener);
    }

    @Override
    public void onBindViewHolder(NoteViewHolder holder, int position) {
        Note note = notes.get(position);
        holder.tvTitle.setText(note.title);
        holder.tvDescription.setText(note.description);
        holder.itemView.setTag(position);
        holder.btnSendEmail.setTag(position);
    }

    @Override
    public int getItemCount() {
        return notes.size();
    }
}
