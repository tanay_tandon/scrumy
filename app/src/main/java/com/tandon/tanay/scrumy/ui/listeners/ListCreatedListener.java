package com.tandon.tanay.scrumy.ui.listeners;

import com.tandon.tanay.scrumy.models.view.SList;

/**
 * Created by tanaytandon on 08/07/17.
 */

public interface ListCreatedListener {

    void onSuccess(SList sList);
}
