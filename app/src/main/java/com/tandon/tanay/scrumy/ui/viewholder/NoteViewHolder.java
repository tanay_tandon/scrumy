package com.tandon.tanay.scrumy.ui.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.tandon.tanay.scrumy.R;

/**
 * Created by tanaytandon on 08/07/17.
 */

public class NoteViewHolder extends RecyclerView.ViewHolder {

    public View itemView;
    public TextView tvTitle;
    public TextView tvDescription;
    public View btnSendEmail;

    public NoteViewHolder(View itemView, View.OnClickListener clickListener,
                          View.OnLongClickListener longClickListener) {
        super(itemView);
        this.itemView = itemView.findViewById(R.id.root_view);
        this.itemView.setOnClickListener(clickListener);
        this.itemView.setOnLongClickListener(longClickListener);
        this.tvTitle = (TextView) itemView.findViewById(R.id.tv_note_title);
        this.tvDescription = (TextView) itemView.findViewById(R.id.tv_note_content);
        this.btnSendEmail = itemView.findViewById(R.id.btn_send_email);
        this.btnSendEmail.setOnClickListener(clickListener);
    }
}
