package com.tandon.tanay.scrumy.models.persistent;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

/**
 * Created by tanaytandon on 08/07/17.
 */

@Entity
public class ListEntity {


    @Id
    private Long id;

    private String name;

    public ListEntity() {
    }

    @Generated(hash = 2098283428)
    public ListEntity(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
