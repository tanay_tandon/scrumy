package com.tandon.tanay.scrumy.ui.activity;

import android.content.ClipData;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.DragEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.tandon.tanay.scrumy.R;
import com.tandon.tanay.scrumy.config.Constants;
import com.tandon.tanay.scrumy.database.FetchDataTask;
import com.tandon.tanay.scrumy.database.UpdateNoteListTask;
import com.tandon.tanay.scrumy.models.view.DataItem;
import com.tandon.tanay.scrumy.models.view.Note;
import com.tandon.tanay.scrumy.models.view.SList;
import com.tandon.tanay.scrumy.ui.adapter.ListPagerAdapter;
import com.tandon.tanay.scrumy.ui.listeners.RemoveNoteListener;
import com.tandon.tanay.scrumy.ui.listeners.DbOperationListener;
import com.tandon.tanay.scrumy.ui.listeners.ListCreatedListener;
import com.tandon.tanay.scrumy.ui.listeners.NoteCreatedListener;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity implements ListCreatedListener,
        NoteCreatedListener, DbOperationListener<List<DataItem>>, RemoveNoteListener,
        View.OnDragListener {

    public static final String TAG = HomeActivity.class.getName();

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private ListPagerAdapter listPagerAdapter;
    private View mainContent;
    private float oldXPosition;
    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private List<DataItem> dataItems = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        mainContent = findViewById(R.id.main_content);

        mainContent.setOnDragListener(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        listPagerAdapter = new ListPagerAdapter(getSupportFragmentManager(), dataItems);

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(listPagerAdapter);

        fetchData();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onNoteCreated(int position, Note note) {
        DataItem dataItem = dataItems.get(position);
        dataItem.notes.add(note);
        listPagerAdapter.notifyDataSetChanged();
    }

    @Override
    public void onSuccess(SList sList) {
        DataItem dataItem = new DataItem(sList.id, sList.name, new ArrayList<Note>());
        dataItems.add(dataItem);
        listPagerAdapter.notifyDataSetChanged();
    }

    @Override
    public void onSuccess(List<DataItem> result) {
        dataItems.addAll(result);
        listPagerAdapter.notifyDataSetChanged();
    }

    private void fetchData() {
        FetchDataTask task = new FetchDataTask(this, this);
        task.execute();
    }

    @Override
    public void removeNoteFromCurrentList(int position, int notePosition) {
        dataItems.get(position).notes.remove(notePosition);
        listPagerAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onDrag(View view, DragEvent dragEvent) {
        int action = dragEvent.getAction();

        if (view.getId() == R.id.main_content) {

            switch (action) {
                case DragEvent.ACTION_DRAG_STARTED: {
                    oldXPosition = dragEvent.getX();
                    return true;
                }

                case DragEvent.ACTION_DRAG_LOCATION: {
                    moveIfNeeded(dragEvent);
                    return true;
                }

                case DragEvent.ACTION_DROP: {
                    retrieveData(dragEvent);
                    return true;
                }
            }
        }
        return false;
    }


    private void moveIfNeeded(DragEvent dragEvent) {
        int currentItem = mViewPager.getCurrentItem();
        float x = dragEvent.getX();
        if (x < 20 && currentItem != 0 && x < (oldXPosition - 10)) {
            mViewPager.setCurrentItem(currentItem - 1);
            oldXPosition = x;
        } else if (x > (mainContent.getWidth() - 40)
                && currentItem < dataItems.size() && x > (oldXPosition + 10)) {
            mViewPager.setCurrentItem(currentItem + 1);
            oldXPosition = x;
        }
    }

    private void retrieveData(DragEvent dragEvent) {
        ClipData clipData = dragEvent.getClipData();
        int currentItem = mViewPager.getCurrentItem();
        if (clipData != null && clipData.getItemCount() > 0) {
            Intent intent = clipData.getItemAt(0).getIntent();
            intent.setExtrasClassLoader(Note.class.getClassLoader());
            Bundle args = intent.getBundleExtra(Constants.Keys.ARGS);
            if (args.containsKey(Constants.Keys.NOTE)
                    && args.containsKey(Constants.Keys.POSITION)) {
                int position = args.getInt(Constants.Keys.POSITION);
                Note note = args.getParcelable(Constants.Keys.NOTE);
                if (currentItem != dataItems.size()) {
                    updateNote(currentItem, note);
                } else {
                    addNoteToOldPosition(position, note);
                }
            }
        }
    }

    private void updateNote(final int currentItem, final Note note) {
        if (currentItem != dataItems.size()) {
            DataItem dataItem = dataItems.get(currentItem);
            note.listId = dataItem.listId;
            UpdateNoteListTask task = new UpdateNoteListTask(this,
                    new DbOperationListener<Boolean>() {
                        @Override
                        public void onSuccess(Boolean result) {
                            dataItems.get(currentItem).notes.add(note);
                            listPagerAdapter.notifyDataSetChanged();
                            Log.d(TAG, "onSuccess: hakunna matata");
                        }
                    });
            task.execute(note);
        }
    }

    private void addNoteToOldPosition(int position, Note note) {
        dataItems.get(position).notes.add(note);
        listPagerAdapter.notifyDataSetChanged();
    }

}
