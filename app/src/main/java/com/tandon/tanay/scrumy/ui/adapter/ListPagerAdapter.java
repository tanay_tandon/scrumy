package com.tandon.tanay.scrumy.ui.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.tandon.tanay.scrumy.models.view.DataItem;
import com.tandon.tanay.scrumy.ui.fragment.CreateNewListFragment;
import com.tandon.tanay.scrumy.ui.fragment.NotesFragment;

import java.util.List;

/**
 * Created by tanaytandon on 08/07/17.
 */

public class ListPagerAdapter extends FragmentStatePagerAdapter {

    private List<DataItem> dataItems;

    public ListPagerAdapter(FragmentManager fragmentManager, List<DataItem> dataItems) {
        super(fragmentManager);
        this.dataItems = dataItems;
    }

    @Override
    public Fragment getItem(int position) {

        if (position == dataItems.size()) {
            return CreateNewListFragment.newInstance();
        }

        return NotesFragment.newInstance(dataItems.get(position), position);
    }

    @Override
    public int getCount() {
        return dataItems.size() + 1;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public float getPageWidth(int position) {
        return 0.95f;
    }
}
