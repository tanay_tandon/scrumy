package com.tandon.tanay.scrumy.ui.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;

import com.tandon.tanay.scrumy.R;
import com.tandon.tanay.scrumy.config.Constants;
import com.tandon.tanay.scrumy.config.Router;
import com.tandon.tanay.scrumy.models.view.Note;

/**
 * Created by tanaytandon on 10/07/17.
 */

public class SendEmailDialog extends BaseDialogFragment implements View.OnClickListener,
        TextWatcher {

    private Note note;
    private String emailId;
    private View btnSendEmail;
    private boolean sendEmailButtonDisabled;

    public static SendEmailDialog newInstance(Note note) {
        SendEmailDialog dialog = new SendEmailDialog();
        Bundle args = new Bundle();
        args.putParcelable(Constants.Keys.NOTE, note);
        dialog.setArguments(args);
        return dialog;
    }


    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        // request a window without the listName
        if (dialog.getWindow() != null) {
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null && args.containsKey(Constants.Keys.NOTE)) {
            note = args.getParcelable(Constants.Keys.NOTE);
        }
        sendEmailButtonDisabled = true;
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_send_note_to, container, false);
        EditText etEmail = (EditText) view.findViewById(R.id.et_email);
        etEmail.addTextChangedListener(this);
        btnSendEmail = view.findViewById(R.id.btn_send_email);
        btnSendEmail.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.btn_send_email: {
                sendEmail();
                break;
            }

            case R.id.btn_cancel: {
                dismissDialog();
                break;
            }
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (sendEmailButtonDisabled && Patterns.EMAIL_ADDRESS.matcher(charSequence).matches()) {
            btnSendEmail.setEnabled(true);
            sendEmailButtonDisabled = false;
        } else if (!sendEmailButtonDisabled &&
                !Patterns.EMAIL_ADDRESS.matcher(charSequence).matches()) {
            btnSendEmail.setEnabled(false);
            sendEmailButtonDisabled = true;
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {
        emailId = editable.toString();
    }

    private void sendEmail() {
        try {
            Router.INSTANCE.sendEmail(getContext(), emailId, note);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        dismissDialog();
    }
}
