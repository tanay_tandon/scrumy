package com.tandon.tanay.scrumy.database;

import android.content.Context;
import android.os.AsyncTask;

import com.tandon.tanay.scrumy.config.AppController;
import com.tandon.tanay.scrumy.models.persistent.DaoSession;
import com.tandon.tanay.scrumy.models.persistent.ListEntity;
import com.tandon.tanay.scrumy.models.persistent.ListEntityDao;
import com.tandon.tanay.scrumy.models.view.SList;
import com.tandon.tanay.scrumy.ui.listeners.DbOperationListener;

/**
 * Created by tanaytandon on 08/07/17.
 */

public class AddNewListTask extends AsyncTask<SList, Void, SList> {

    private Context context;
    private DbOperationListener<SList> dbOperationListener;

    public AddNewListTask(Context context, DbOperationListener<SList> dbOperationListener) {
        this.context = context;
        this.dbOperationListener = dbOperationListener;
    }

    @Override
    protected SList doInBackground(SList... sLists) {
        SList newSList = null;
        if (sLists != null && sLists.length > 0) {
            SList sList = sLists[0];
            DaoSession daoSession = ((AppController) context.getApplicationContext()).getDaoSession();
            ListEntityDao listEntityDao = daoSession.getListEntityDao();
            ListEntity listEntity = new ListEntity();
            listEntity.setName(sList.name);
            long listId = listEntityDao.insert(listEntity);
            newSList = new SList(listId, listEntity.getName());
        }
        return newSList;
    }

    @Override
    protected void onPostExecute(SList sList) {
        dbOperationListener.onSuccess(sList);
    }
}
