package com.tandon.tanay.scrumy.ui.fragment;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tandon.tanay.scrumy.R;
import com.tandon.tanay.scrumy.config.Constants;
import com.tandon.tanay.scrumy.models.view.SList;
import com.tandon.tanay.scrumy.ui.dialog.AddNewListDialog;
import com.tandon.tanay.scrumy.ui.listeners.ListCreatedListener;

public class CreateNewListFragment extends BaseFragment implements View.OnClickListener {

    public static final String TAG = CreateNewListFragment.class.getName();

    private ListCreatedListener listCreatedListener;

    public static CreateNewListFragment newInstance() {
        CreateNewListFragment fragment = new CreateNewListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup parent,
                             Bundle savedInstanceState) {
        View view = layoutInflater.inflate(R.layout.fragment_new_list, parent, false);
        view.findViewById(R.id.btn_add_list).setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.btn_add_list: {
                showAddListDialog();
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listCreatedListener = (ListCreatedListener) context;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case Constants.RequestCodes.NEW_LIST_CREATED: {
                    SList sList = data.getParcelableExtra(Constants.Keys.SLIST);
                    if (listCreatedListener != null) {
                        listCreatedListener.onSuccess(sList);
                    }
                }
            }
        }
    }


    private void showAddListDialog() {
        AddNewListDialog dialog = AddNewListDialog.newInstance();
        dialog.setTargetFragment(this, Constants.RequestCodes.NEW_LIST_CREATED);
        dialog.show(getFragmentManager(), TAG);
    }
}
