package com.tandon.tanay.scrumy.ui.fragment;

import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.View;

/**
 * Created by tanaytandon on 08/07/17.
 */

public abstract class BaseFragment extends Fragment {

    protected void showLongDurationSnackbar(View rootView, String message) {
        Snackbar.make(rootView, message, Snackbar.LENGTH_LONG).show();
    }

    protected void showInfiniteDurationSnackbar(View rootView, String message) {
        Snackbar.make(rootView, message, Snackbar.LENGTH_INDEFINITE).show();
    }

}
