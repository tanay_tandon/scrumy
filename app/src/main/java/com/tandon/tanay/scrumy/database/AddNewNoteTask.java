package com.tandon.tanay.scrumy.database;

import android.content.Context;
import android.os.AsyncTask;

import com.tandon.tanay.scrumy.config.AppController;
import com.tandon.tanay.scrumy.models.persistent.DaoSession;
import com.tandon.tanay.scrumy.models.persistent.NoteEntity;
import com.tandon.tanay.scrumy.models.persistent.NoteEntityDao;
import com.tandon.tanay.scrumy.models.view.Note;
import com.tandon.tanay.scrumy.ui.listeners.DbOperationListener;

/**
 * Created by tanaytandon on 08/07/17.
 */

public class AddNewNoteTask extends AsyncTask<Note, Void, Note> {

    private Context context;
    private DbOperationListener<Note> dbOperationListener;

    public AddNewNoteTask(Context context, DbOperationListener<Note> dbOperationListener) {
        this.context = context;
        this.dbOperationListener = dbOperationListener;
    }

    @Override
    protected Note doInBackground(Note... notes) {
        Note newNote = null;
        if (notes != null && notes.length > 0) {
            Note note = notes[0];
            DaoSession daoSession = ((AppController) context.getApplicationContext()).getDaoSession();
            NoteEntity noteEntity = new NoteEntity();
            noteEntity.setListId(note.listId);
            noteEntity.setTitle(note.title);
            noteEntity.setDescription(note.description);
            NoteEntityDao noteEntityDao = daoSession.getNoteEntityDao();
            long noteId = noteEntityDao.insert(noteEntity);
            newNote = note;
            newNote.id = noteId;
        }
        return newNote;
    }

    @Override
    public void onPostExecute(Note note) {
        dbOperationListener.onSuccess(note);
    }
}
