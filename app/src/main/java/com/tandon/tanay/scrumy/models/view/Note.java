package com.tandon.tanay.scrumy.models.view;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by tanaytandon on 08/07/17.
 */

public class Note implements Parcelable {

    public String title;
    public String description;
    public Long id;
    public Long listId;

    public Note(String title, String description, Long listId) {
        this.listId = listId;
        this.title = title;
        this.description = description;
    }

    public Note(String title, String description, Long id, Long listId) {
        this.id = id;
        this.listId = listId;
        this.title = title;
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        Note note = null;
        if (o != null) {
            note = (Note) o;
        }
        return note != null && note.id != null && note.id.equals(this.id);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.description);
        dest.writeValue(this.id);
        dest.writeValue(this.listId);
    }

    protected Note(Parcel in) {
        this.title = in.readString();
        this.description = in.readString();
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.listId = (Long) in.readValue(Long.class.getClassLoader());
    }

    public static final Parcelable.Creator<Note> CREATOR = new Parcelable.Creator<Note>() {
        @Override
        public Note createFromParcel(Parcel source) {
            return new Note(source);
        }

        @Override
        public Note[] newArray(int size) {
            return new Note[size];
        }
    };
}
