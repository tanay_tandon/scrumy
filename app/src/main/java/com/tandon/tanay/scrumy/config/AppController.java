package com.tandon.tanay.scrumy.config;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;

import com.tandon.tanay.scrumy.models.persistent.DaoMaster;
import com.tandon.tanay.scrumy.models.persistent.DaoSession;
import com.tandon.tanay.scrumy.models.persistent.NoteEntityDao;


public class AppController extends Application {

    private DaoSession daoSession;

    public void onCreate() {
        super.onCreate();

        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, Constants.Keys.DATABASE_NAME);
        SQLiteDatabase database = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(database);
        daoSession = daoMaster.newSession();
    }

    public DaoSession getDaoSession() {
        return daoSession;
    }

}
