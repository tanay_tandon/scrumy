package com.tandon.tanay.scrumy.models.view;

import android.os.Parcel;
import android.os.Parcelable;

public class SList implements Parcelable {

    public String name;

    public long id;

    public SList(String name) {
        this.name = name;
    }

    public SList(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeLong(this.id);
    }

    protected SList(Parcel in) {
        this.name = in.readString();
        this.id = in.readLong();
    }

    public static final Parcelable.Creator<SList> CREATOR = new Parcelable.Creator<SList>() {
        @Override
        public SList createFromParcel(Parcel source) {
            return new SList(source);
        }

        @Override
        public SList[] newArray(int size) {
            return new SList[size];
        }
    };
}
