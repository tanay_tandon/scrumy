package com.tandon.tanay.scrumy.config;

/**
 * Created by tanaytandon on 08/07/17.
 */

public class Constants {

    public interface Keys {
        String APP_NAME = "Scrumy";
        String DATABASE_NAME = "Scrumy";
        String DATA_ITEM = "DataItem";
        String NOTE = "Note";
        String SLIST = "SLIst";
        String POSITION = "position";
        String ARGS = "Args";
        String NOTE_INDEX = "note_index";
        String LIST_NAME = "listName";
    }

    public interface RequestCodes {
        int NEW_LIST_CREATED = 1;
        int NEW_NOTE_CREATED = 2;
    }
}
