package com.tandon.tanay.scrumy.ui.listeners;


public interface DbOperationListener<T> {

    void onSuccess(T result);
}
