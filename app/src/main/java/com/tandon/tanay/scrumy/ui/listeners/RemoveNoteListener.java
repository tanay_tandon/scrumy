package com.tandon.tanay.scrumy.ui.listeners;

import com.tandon.tanay.scrumy.models.view.Note;

/**
 * Created by tanaytandon on 09/07/17.
 */

public interface RemoveNoteListener {

    void removeNoteFromCurrentList(int position, int notePosition);
}
