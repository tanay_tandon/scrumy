package com.tandon.tanay.scrumy.models.persistent;

import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.ToOne;

/**
 * Created by tanaytandon on 08/07/17.
 */

@Entity
public class NoteEntity {

    @Id
    private Long id;

    private String title;

    private String description;

    private Long listId;

    @ToOne(joinProperty = "listId")
    private ListEntity listEntity;

    /**
     * Used to resolve relations
     */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    /**
     * Used for active entity operations.
     */
    @Generated(hash = 765597747)
    private transient NoteEntityDao myDao;

    @Generated(hash = 846811010)
    private transient Long listEntity__resolvedKey;

    public NoteEntity() {
    }

    @Generated(hash = 886314806)
    public NoteEntity(Long id, String title, String description, Long listId) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.listId = listId;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return this.id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return this.title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return this.description;
    }

    public Long getListId() {
        return this.listId;
    }

    public void setListId(Long listId) {
        this.listId = listId;
    }

    /**
     * To-one relationship, resolved on first access.
     */
    @Generated(hash = 1883487647)
    public ListEntity getListEntity() {
        Long __key = this.listId;
        if (listEntity__resolvedKey == null
                || !listEntity__resolvedKey.equals(__key)) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            ListEntityDao targetDao = daoSession.getListEntityDao();
            ListEntity listEntityNew = targetDao.load(__key);
            synchronized (this) {
                listEntity = listEntityNew;
                listEntity__resolvedKey = __key;
            }
        }
        return listEntity;
    }

    /**
     * called by internal mechanisms, do not call yourself.
     */
    @Generated(hash = 1635412224)
    public void setListEntity(ListEntity listEntity) {
        synchronized (this) {
            this.listEntity = listEntity;
            listId = listEntity == null ? null : listEntity.getId();
            listEntity__resolvedKey = listId;
        }
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 478714418)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getNoteEntityDao() : null;
    }

}
