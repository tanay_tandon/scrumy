package com.tandon.tanay.scrumy.ui.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;

import com.tandon.tanay.scrumy.R;
import com.tandon.tanay.scrumy.config.Constants;
import com.tandon.tanay.scrumy.database.AddNewListTask;
import com.tandon.tanay.scrumy.models.view.SList;
import com.tandon.tanay.scrumy.ui.listeners.DbOperationListener;

/**
 * Created by tanaytandon on 08/07/17.
 */

public class AddNewListDialog extends BaseDialogFragment implements View.OnClickListener,
        TextWatcher, DbOperationListener<SList> {

    private boolean addListButtonDisabled = true;
    private View btnAddNewList;
    private String listName;

    public static AddNewListDialog newInstance() {
        AddNewListDialog dialog = new AddNewListDialog();
        return dialog;
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        // request a window without the listName
        if (dialog.getWindow() != null) {
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        return dialog;
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_add_new_list, container, false);
        btnAddNewList = view.findViewById(R.id.btn_add_list);
        btnAddNewList.setEnabled(false);
        btnAddNewList.setOnClickListener(this);
        view.findViewById(R.id.btn_cancel).setOnClickListener(this);
        EditText editText = (EditText) view.findViewById(R.id.et_list_name);
        editText.addTextChangedListener(this);
        return view;
    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {

            case R.id.btn_add_list: {
                createNewList();
                break;
            }

            case R.id.btn_cancel: {
                dismissDialog();
                break;
            }
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (addListButtonDisabled && charSequence.length() > 0) {
            addListButtonDisabled = false;
            btnAddNewList.setEnabled(true);
        } else if (!addListButtonDisabled && charSequence.length() == 0) {
            addListButtonDisabled = true;
            btnAddNewList.setEnabled(false);
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {
        listName = editable.toString();
    }

    private void createNewList() {
        // redundant check
        if (listName.length() > 0 && getContext() != null) {
            AddNewListTask task = new AddNewListTask(getContext(), this);
            SList sList = new SList(listName);
            task.execute(sList);
        }
    }

    @Override
    public void onSuccess(SList result) {
        Intent data = new Intent();
        data.putExtra(Constants.Keys.SLIST, result);
        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, data);
        dismissDialog();
    }
}
