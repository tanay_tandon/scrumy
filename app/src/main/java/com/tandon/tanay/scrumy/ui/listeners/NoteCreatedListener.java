package com.tandon.tanay.scrumy.ui.listeners;

import com.tandon.tanay.scrumy.models.view.Note;

/**
 * Created by tanaytandon on 08/07/17.
 */

public interface NoteCreatedListener {

    void onNoteCreated(int position, Note note);
}
