package com.tandon.tanay.scrumy.config;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.tandon.tanay.scrumy.models.view.Note;

/**
 * Created by tanaytandon on 10/07/17.
 */

public enum Router {
    INSTANCE;

    public void sendEmail(Context context, String sendTo, Note note) {
        Intent sendIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", sendTo, null));
        sendIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{sendTo});
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, note.title);
        sendIntent.putExtra(Intent.EXTRA_TEXT, note.description);
        context.startActivity(Intent.createChooser(sendIntent, "Send email..."));

    }
}
