package com.tandon.tanay.scrumy.ui.dialog;


import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import com.tandon.tanay.scrumy.R;
import com.tandon.tanay.scrumy.config.Constants;
import com.tandon.tanay.scrumy.database.AddNewNoteTask;
import com.tandon.tanay.scrumy.models.view.Note;
import com.tandon.tanay.scrumy.models.view.SList;
import com.tandon.tanay.scrumy.ui.listeners.DbOperationListener;


public class AddNewNoteDialog extends BaseDialogFragment implements TextWatcher,
        DbOperationListener<Note>, View.OnClickListener {

    private View btnAddNewNote;
    private EditText etNoteTitle, etNoteDescription;
    private TextView tvListName;
    private SList sList;
    private boolean addNewNoteButtonDisabled = true;
    private String noteTitle;

    public static AddNewNoteDialog newInstance(SList sList) {
        AddNewNoteDialog dialog = new AddNewNoteDialog();
        Bundle args = new Bundle();
        args.putParcelable(Constants.Keys.SLIST, sList);
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        // request a window without the listName
        if (dialog.getWindow() != null) {
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        return dialog;
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_add_new_note, container, false);
        btnAddNewNote = view.findViewById(R.id.btn_add_new_note);
        btnAddNewNote.setEnabled(false);
        btnAddNewNote.setOnClickListener(this);
        etNoteTitle = (EditText) view.findViewById(R.id.et_note_title);
        etNoteDescription = (EditText) view.findViewById(R.id.et_note_description);
        tvListName = (TextView) view.findViewById(R.id.tv_list_name);

        Bundle args = getArguments();

        if (args != null && args.containsKey(Constants.Keys.SLIST)) {
            sList = args.getParcelable(Constants.Keys.SLIST);
            if (sList != null) {
                tvListName.setText(sList.name);
            }
        }

        view.findViewById(R.id.btn_cancel).setOnClickListener(this);

        etNoteTitle.addTextChangedListener(this);

        return view;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (addNewNoteButtonDisabled && charSequence.length() > 0) {
            addNewNoteButtonDisabled = false;
            btnAddNewNote.setEnabled(true);
        } else if (!addNewNoteButtonDisabled && charSequence.length() == 0) {
            addNewNoteButtonDisabled = true;
            btnAddNewNote.setEnabled(false);
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {
        noteTitle = editable.toString();
    }

    @Override
    public void onSuccess(Note result) {
        Intent intent = new Intent();
        intent.putExtra(Constants.Keys.NOTE, result);
        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
        dismissDialog();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.btn_add_new_note: {
                createNewNote();
                break;
            }

            case R.id.btn_cancel: {
                dismissDialog();
                break;
            }
        }
    }

    private void createNewNote() {
        String description = etNoteDescription.getText().toString();
        AddNewNoteTask task = new AddNewNoteTask(getContext(), this);
        Note note = new Note(noteTitle, description, sList.id);
        task.execute(note);
    }
}
